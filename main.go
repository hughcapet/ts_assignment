package main

import (
	"context"
	"encoding/csv"
	"fmt"
	"hash/fnv"
	"log"
	"os"
	"sort"
	"strings"
	"sync/atomic"
	"time"

	"github.com/jackc/pgx/v5"
	"github.com/urfave/cli/v2"
	"golang.org/x/sync/errgroup"
)

// TODO: check timestamp interval condition
const queryText = "select time_bucket('1 minute', ts) as bucket, min(usage), max(usage) from cpu_usage where host=$1 and ts>=$2 and ts<=$3 group by bucket;"

type ExecTask struct {
	QueryNum int
	Host     string
	FromTs   string
	ToTs     string
}

type ExecResult struct {
	QueryNum      int
	ExecutionTime time.Duration
}

type ExecResultAggs struct {
	ExecCount      int
	MaxExecTime    time.Duration
	MinExecTime    time.Duration
	AvgExecTime    time.Duration
	MedianExecTime time.Duration
}

func benchmarkQuery(host, fromTs, toTs string, conn *pgx.Conn) (time.Duration, error) {
	start := time.Now()

	_, err := conn.Exec(context.Background(), queryText, host, fromTs, toTs)
	if err != nil {
		return time.Since(start), err // TODO: do we need time of the erroneous query counted?
	}
	return time.Since(start), nil
}

func getWorkerId(hostName string, numWorkers uint64) uint64 {
	h := fnv.New64a()
	h.Write([]byte(hostName))
	return h.Sum64() % numWorkers
}

func getResultAggregates(resChannel <-chan ExecResult) (resAggs ExecResultAggs) {
	var totalTime time.Duration

	resArray := make([]ExecResult, 0)
	for res := range resChannel {
		resArray = append(resArray, res)
		totalTime += res.ExecutionTime
	}
	sort.SliceStable(resArray, func(i, j int) bool {
		return resArray[i].ExecutionTime < resArray[j].ExecutionTime
	})

	resAggs.ExecCount = len(resArray)
	resAggs.AvgExecTime = time.Duration(int64(totalTime) / int64(resAggs.ExecCount))
	resAggs.MinExecTime = resArray[0].ExecutionTime
	resAggs.MaxExecTime = resArray[resAggs.ExecCount-1].ExecutionTime
	if resAggs.ExecCount%2 == 0 {
		resAggs.MedianExecTime = resArray[resAggs.ExecCount/2].ExecutionTime + resArray[resAggs.ExecCount/2-1].ExecutionTime
	} else {
		resAggs.MedianExecTime = resArray[resAggs.ExecCount/2].ExecutionTime
	}
	return
}

func runBenchmark(configFile string, nworkers uint64, verbose bool) (err error) {
	var reader *csv.Reader
	if configFile == "" {
		reader = csv.NewReader(os.Stdin)
	} else {
		f, err := os.OpenFile(configFile, os.O_RDONLY, 0600)
		if err != nil {
			return fmt.Errorf("Error while openning file: %v", err)
		}
		defer f.Close()
		reader = csv.NewReader(f)
	}

	fieldNames, err := reader.Read()
	if err != nil {
		return fmt.Errorf("failed to read header line: %v", err)
	}
	headerLen := len(fieldNames)
	if headerLen != 3 {
		return fmt.Errorf("Unexpected number of fields: expected 3, got %d", headerLen)
	}

	lines, err := reader.ReadAll()
	if err != nil {
		return fmt.Errorf("failed to read file: %v", err)
	}

	// create lists of jobs for every worker
	queryChannels := make([]chan ExecTask, nworkers)
	for i := uint64(0); i < nworkers; i++ {
		queryChannels[i] = make(chan ExecTask, len(lines))
	}
	for n, line := range lines {
		queryChannels[getWorkerId(line[0], nworkers)] <- ExecTask{
			n,
			strings.TrimSpace(line[0]),
			strings.TrimSpace(line[1]),
			strings.TrimSpace(line[2]),
		}
	}

	var numErrors uint32
	resultsChannel := make(chan ExecResult, len(lines))
	gr, ctx := errgroup.WithContext(context.Background())

	// run queries in parallel and store results
	start := time.Now()
	for i := uint64(0); i < nworkers; i++ {
		i := i
		gr.Go(func() error {
			// if connection fails, exit immediately and cancel the whole group
			pgConn, err := pgx.Connect(ctx, os.Getenv("BENCHMARK_DSN"))
			if err != nil {
				return fmt.Errorf("worker %d: Postgres connection failed: %v", i, err)
			}
			defer pgConn.Close(ctx)

			// do our work
			for query := range queryChannels[i] {
				// if a single query fails, update counters and continue
				executionTime, err := benchmarkQuery(query.Host, query.FromTs, query.ToTs, pgConn)
				if err != nil {
					atomic.AddUint32(&numErrors, 1)
					resultsChannel <- ExecResult{query.QueryNum, executionTime} // TODO: do we need time of the erroneous query counted?
					log.Printf("%dth worker failed: %v\n", i, err)
					continue
				}
				resultsChannel <- ExecResult{query.QueryNum, executionTime}
			}
			return nil
		})
	}

	for i := uint64(0); i < nworkers; i++ {
		close(queryChannels[i])
	}
	if err := gr.Wait(); err != nil {
		return err
	}
	processingTime := time.Since(start)

	close(resultsChannel)
	if len(resultsChannel) > 0 {
		result := getResultAggregates(resultsChannel)

		fmt.Printf(`# of queries processed: %d
# of failed queries: %d
total processing time: %s
the minimum query time: %s
the maximum query time: %s
the average query time: %s
the median query time: %s
`, result.ExecCount,
			numErrors,
			processingTime,
			result.MinExecTime,
			result.MaxExecTime,
			result.AvgExecTime,
			result.MedianExecTime)
	} else {
		return fmt.Errorf("empty result set")
	}

	return nil
}

func main() {
	var configFile string
	var nworkers uint64
	var logVerbose bool

	app := &cli.App{
		Name:  "benchmark",
		Usage: "SELECT queries benchmarking",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "config",
				Aliases:     []string{"c"},
				Usage:       "configuration file with the query parameters. If not provided, expects query parameters to be provided via stdin.",
				Destination: &configFile,
			},
			&cli.Uint64Flag{
				Name:    "workers",
				Aliases: []string{"w"},
				Usage:   "number of concurrent workers",
				Value:   1,
				Action: func(ctx *cli.Context, v uint64) error {
					if v < 1 {
						return fmt.Errorf("Number of workers should be positive")
					}
					return nil
				},
				Destination: &nworkers,
			},
			&cli.BoolFlag{
				Name:        "verbose",
				Aliases:     []string{"v"},
				Usage:       "enable verbose logging",
				Value:       false,
				Destination: &logVerbose,
			},
		},
		Action: func(ctx *cli.Context) error {
			if logVerbose {
				log.Printf("config is read from %s, %d worker(s) will be used\n", configFile, nworkers)
			}
			return runBenchmark(configFile, nworkers, logVerbose)
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
