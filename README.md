# SELECT benchmarking tool

A command line tool that can be used to benchmark the specific SELECT query performance across multiple workers/clients against a TimescaleDB instance.

Currently only the following query is tested: `select time_bucket('1 minute', ts) as bucket, min(usage), max(usage) from cpu_usage where host=$1 and ts>=$2 and ts<=$3 group by bucket;`, where parameters are passed via a *.csv comma-separated file.

## How to run

### Run in a docker container

1. Build the docker image:
```console
docker build -t ts .
```
The vesrions of PostgreSQL and TimescaleDB are configurable via the build arguments:
```console
docker build --build-arg PG_MAJOR=$PGVERSION --build-arg TIMESCALEDB_VER=$TSVERSION -t ts .
```
By default the image includes PostgreSQL 15 and TimescaleDB 2.11.1.

2. Run image locally after the build:
```console
docker run --name ts_benchmark -it ts
```
or in a detached mode
```console
docker run --name ts_benchmark -it -d ts
```

3. Run the tool

```console
docker exec -it ts_benchmark bash -c "PGUSER=postgres ./benchmark_ts -c helper_scripts/query_params.csv"
# of queries processed: 200
# of failed queries: 0
total processing time: 162.266982ms
the minimum query time: 636.42µs
the maximum query time: 9.327547ms
the average query time: 790.64µs
the median query time: 1.344006ms
```

or under the PostgreSQL superuser
```console
docker exec -u postgres -it ts_benchmark bash -c "PGUSER=postgres ./benchmark_ts -c helper_scripts/query_params.csv"
# of queries processed: 200
# of failed queries: 0
total processing time: 143.248447ms
the minimum query time: 596.334µs
the maximum query time: 5.419384ms
the average query time: 702.497µs
the median query time: 1.299502ms
```

passing parameters via STDIN 
```console
docker exec -it ts_benchmark bash -c "cat helper_scripts/query_params.csv | PGUSER=postgres ./benchmark_ts"
# of queries processed: 200
# of failed queries: 0
total processing time: 134.221549ms
the minimum query time: 522.878µs
the maximum query time: 8.416667ms
the average query time: 641.233µs
the median query time: 1.145173ms
```

### Run locally

1. Build the tool
```console
go get -d
go build
```

2. Setup PostgreSQL with TimescaleDB installed and loaded

3. Run the tool
```console
BENCHMARK_DSN="<DSN or URI connection string>" ./benchmark_ts
```

## Configuration parameters

```
./benchmark_ts --help
NAME:
   benchmark - SELECT queries benchmarking

USAGE:
   benchmark [global options] command [command options] [arguments...]

COMMANDS:
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --config value, -c value   configuration file with the query parameters. If not provided, expects query parameters to be provided via stdin.
   --workers value, -w value  number of concurrent workers (default: 1)
   --verbose, -v              enable verbose logging (default: false)
   --help, -h                 show help
```

PostgreSQL connection parameters can be set either via standard libpq environment variables or by setting the special ``BENCHMARK_DSN`` evironment variable to the connection DSN or URI string.

## How to test

You can run a set of simple tests using the built image:
```console
bash ./tests/test_benchmark.sh ts ts_benchmark
```
where the first argument is the name of the image, the second - the name of the container to run.
