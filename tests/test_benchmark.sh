#!/bin/bash

if ! docker info &> /dev/null; then
    if podman info &> /dev/null; then
        alias docker=podman
        shopt -s expand_aliases
    else
        echo "docker/podman: command not found"
        exit 1
    fi
fi


readonly RED="\033[1;31m"
readonly RESET="\033[0m"
readonly GREEN="\033[0;32m"

function log_info() {
    echo -e "${GREEN}$*${RESET}"
}

function log_error() {
    echo -e "${RED}$*${RESET}"
    exit 1
}

[[ $# -eq 2 ]] || log_error "Script requires two arguments: image name and container name"

readonly TEST_IMAGE="$1"
readonly TEST_CONTAINER="$2"

function start_container() {
    docker run --rm -d --name "$TEST_CONTAINER" "$TEST_IMAGE"
}

function stop_container() {
    docker rm -f "$TEST_CONTAINER" 1>/dev/null
}

function wait_setup() {
    while ! docker_exec psql -d homework 2>/dev/null; do
        sleep 1
    done
}

function docker_exec() {
    docker exec "$TEST_CONTAINER" su postgres -c "$1"
}

function run_test() {
    "$@" || log_error "Test case $1 FAILED"
    echo -e "Test case $1 ${GREEN}PASSED${RESET}"
}

function test_read_from_stdin() {
    docker_exec "cat helper_scripts/query_params.csv | ./benchmark_ts -v"
}

function test_read_from_config_file() {
    docker_exec "./benchmark_ts -v -c helper_scripts/query_params.csv"
}

function test_set_wrong_concurrency() {
    ! docker_exec "./benchmark_ts -v -c helper_scripts/query_params.csv -w -1"
    docker_exec "./benchmark_ts -v -c helper_scripts/query_params.csv -w 0" 2>&1 | grep "Number of workers should be positive"
}

function test_set_concurrency() {
    docker_exec "./benchmark_ts -v -c helper_scripts/query_params.csv -w 3" 2>&1 | grep "config is read from helper_scripts/query_params.csv, 3 worker(s)"
}

function test_set_wrong_config_file() {
    ! docker_exec "./benchmark_ts -v -c /wrong/file"
}

function run_empty_config_no_header() {
    docker_exec "touch /tmp/test.csv"
    docker_exec "./benchmark_ts -v -c /tmp/test.csv" 2>&1 | grep "failed to read header line"
}

function run_config_wrong_header_len() {
    docker_exec "echo 'something,absolutely,not, relevant' > /tmp/test.csv && tail -n 100 helper_scripts/query_params.csv >> /tmp/test.csv"
    docker_exec "./benchmark_ts -v -c /tmp/test.csv" 2>&1 | grep "Unexpected number of fields: expected 3, got 4"
}

function run_wrong_delimiter() {
    docker_exec "sed -i 's/,/;/g' /tmp/test.csv"
    docker_exec "./benchmark_ts -v -c /tmp/test.csv" 2>&1 | grep "Unexpected number of fields: expected 3, got 1"
}

function run_empty_config() {
    docker_exec "echo 'hostname,start_time,end_time' > /tmp/test.csv"
    docker_exec "./benchmark_ts -v -c /tmp/test.csv" 2>&1 | grep "empty result set"
}

function main() {
    start_container
    wait_setup

    run_test test_read_from_stdin
    run_test test_read_from_config_file
    run_test test_set_concurrency
    run_test test_set_wrong_concurrency
    run_test test_set_wrong_config_file
    run_test run_empty_config_no_header
    run_test run_config_wrong_header_len
    run_test run_empty_config
    run_test run_wrong_delimiter

    log_info "All tests passed"

    stop_container
}

trap stop_container QUIT TERM EXIT

main
