ARG PG_MAJOR=15
ARG TIMESCALEDB_VER=2.11.1
ARG PGHOME=/home/postgres

FROM ubuntu:22.04 as builder

ARG PG_MAJOR
ARG TIMESCALEDB_VER
ARG PGHOME

ENV PG_MAJOR=$PG_MAJOR \
    TIMESCALEDB_VER=$TIMESCALEDB_VER \
    PGHOME=$PGHOME

WORKDIR /tmp

RUN mkdir benchmark_build
COPY go.mod go.sum main.go benchmark_build/

COPY build/build_script.sh .
RUN bash ./build_script.sh

FROM scratch
COPY --from=builder / /

ARG PG_MAJOR
ARG PGHOME

ENV PATH=$PATH:/usr/lib/postgresql/${PG_MAJOR}/bin \
    PGHOME=$PGHOME \
    PGROOT=$PGHOME/pgdata/pgroot \
    PGDATA=$PGROOT/data \
    PGDATABASE=homework

COPY helper_scripts $PGHOME/helper_scripts
COPY build/launch.sh /

WORKDIR $PGHOME

ENTRYPOINT ["/bin/sh", "/launch.sh"]
