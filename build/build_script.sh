#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

set -ex

apt-get update -y
apt-get install -y gnupg lsb-release curl ca-certificates

# Install postgres
DISTRIB_CODENAME=$(sed -n 's/DISTRIB_CODENAME=//p' /etc/lsb-release)
echo "deb http://apt.postgresql.org/pub/repos/apt/ ${DISTRIB_CODENAME}-pgdg main" >> /etc/apt/sources.list.d/pgdg.list
curl -s -o - https://www.postgresql.org/media/keys/ACCC4CF8.asc | gpg --dearmor > /etc/apt/trusted.gpg.d/apt.postgresql.org.gpg
apt-get update -y
apt-get install -y "postgresql-${PG_MAJOR}"

# Install timescaledb
echo "deb https://packagecloud.io/timescale/timescaledb/ubuntu/ ${DISTRIB_CODENAME} main" | tee /etc/apt/sources.list.d/timescaledb.list
curl -L https://packagecloud.io/timescale/timescaledb/gpgkey | gpg --dearmor > /etc/apt/trusted.gpg.d/timescaledb.gpg
apt-get update -y
apt-get install -y "timescaledb-2-postgresql-${PG_MAJOR}=${TIMESCALEDB_VER}*" "timescaledb-2-loader-postgresql-${PG_MAJOR}=${TIMESCALEDB_VER}*"

# Build benchmarking tool
apt-get install -y golang-go
(
    cd benchmark_build
    go get -d
    go build -o $PGHOME/benchmark_ts
)

# Auxiliary stuff
apt-get install -y git cmake
git clone https://github.com/ncopa/su-exec.git
make -C su-exec LDFLAGS=-s all
cp su-exec/su-exec /usr/sbin

# Clean up
apt-get purge -y --allow-remove-essential \
    e2fsprogs \
    curl \
    python3 \
    gzip \
    ncurses-bin \
    e2fslibs \
    bsdutils
apt-get autoremove -y 
apt-get clean

rm -rf /tmp/* \
       /var/lib/apt/lists/* \
       /root/.cache /var/cache/debconf/*
