#!/bin/bash

export PGUSER=postgres
cd "$PGHOME" || exit

mkdir -p "$PGDATA"
chown $PGUSER:$PGUSER "$PGDATA"
su-exec $PGUSER initdb "$PGDATA"
cat > "$PGDATA/pg_hba.conf" << EOF
local       all         all                                     trust
host        all         all             127.0.0.1/32            md5
host        all         all             ::1/128                 md5
EOF

{
    while ! pg_isready; do
        sleep 1
    done

    su-exec $PGUSER psql -d postgres -f helper_scripts/cpu_usage.sql
    su-exec $PGUSER psql -c "\COPY cpu_usage FROM helper_scripts/cpu_usage.csv CSV HEADER"
}&

exec su-exec $PGUSER postgres -D "$PGDATA" --log_filename=pg.log --log_file_mode=0644 --shared_preload_libraries=timescaledb
